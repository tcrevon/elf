module Lib
    ( someFunc
    ) where

import Header

import Data.Binary.Get
import qualified Data.ByteString.Lazy as BL

someFunc :: IO ()
someFunc = do
    input <- BL.getContents
    print $ runGet parseHeader input
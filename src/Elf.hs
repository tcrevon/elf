{-# LANGUAGE MultiParamTypeClasses #-}

module Elf (
    fromWord,
    Endianness(..),
    parseEndianness,
    WordSize(..),
    parseWordSize,
    Address(..),
    Offset(..),
    Size(..),
    WordVariant(..),
    Parse(..),
    ParseVariant(..)
) where

import Data.Binary.Get
import Data.Word  


fromWord :: (Integral a, Enum b) => a -> b
fromWord = toEnum . fromIntegral

data Endianness = 
    LittleEndian 
    | BigEndian
    deriving Eq

instance Enum Endianness where
    toEnum n =
        case n of
            1 -> LittleEndian
            2 -> BigEndian

    fromEnum n =
        case n of
            LittleEndian -> 1
            BigEndian -> 2

instance Show Endianness where
    show LittleEndian = "little endian"
    show BigEndian = "big endian"

parseEndianness :: Get Endianness
parseEndianness = fromWord <$> getWord8

data WordSize =
    Bit32
    | Bit64
    deriving Eq

instance Enum WordSize where
    toEnum n =
        case n of
            1 -> Bit32
            2 -> Bit64

    fromEnum n =
        case n of
            Bit32 -> 1
            Bit64 -> 2

instance Show WordSize where
    show Bit32 = "32 bits"
    show Bit64 = "64 bits"

parseWordSize :: Get WordSize
parseWordSize = fromWord <$> getWord8

class Parse a b where
    parse :: a -> Get b

class ParseVariant a where
    parseVariant :: WordSize -> Endianness -> Get a

data Address =
    Address32 Word32
  | Address64 Word64
  deriving (Eq, Show)

instance ParseVariant Address where
    parseVariant Bit32 BigEndian = Address32 <$> getWord32be
    parseVariant Bit64 BigEndian = Address64 <$> getWord64be
    parseVariant Bit32 LittleEndian = Address32 <$> getWord32le
    parseVariant Bit64 LittleEndian = Address64 <$> getWord64le
     
data Offset =
    Offset32 Word32
  | Offset64 Word64
  deriving (Eq, Show)

instance ParseVariant Offset where
    parseVariant Bit32 BigEndian = Offset32 <$> getWord32be
    parseVariant Bit64 BigEndian = Offset64 <$> getWord64be
    parseVariant Bit32 LittleEndian = Offset32 <$> getWord32le
    parseVariant Bit64 LittleEndian = Offset64 <$> getWord64le

data Size =
    Size32 Word32
  | Size64 Word64
  deriving (Eq, Show)

instance ParseVariant Size where
    parseVariant Bit32 BigEndian = Size32 <$> getWord32be
    parseVariant Bit64 BigEndian = Size64 <$> getWord64be
    parseVariant Bit32 LittleEndian = Size32 <$> getWord32le
    parseVariant Bit64 LittleEndian = Size64 <$> getWord64le

data WordVariant =
    MediumUnsignedInteger Word16    -- Half
  | UnsignedInteger Word32          -- Word
  | SignedInteger Word32            -- SWord
  | UnsignedLongInteger Word64      -- XWord
  | SignedLongInteger Word64        -- SXWord
  deriving (Eq, Show)

module ProgramHeader
( parseProgram
) where

import Elf

import Data.Binary.Get    
import Data.Word

data SegmentType =
    Null
  | Load
  | Dynamic
  | Interp
  | Note
  | Shlib
  | Phdr
  | Loos
  | Hios
  | Loproc
  | Hiproc
  deriving (Eq, Show)

instance Enum SegmentType where
  toEnum s = case s of
    0x00000000 -> Null
    0x00000001 -> Load
    0x00000002 -> Dynamic
    0x00000003 -> Interp
    0x00000004 -> Note
    0x00000005 -> Shlib
    0x00000006 -> Phdr
    0x60000000 -> Loos
    0x6FFFFFFF -> Hios
    0x70000000 -> Loproc
    0x7FFFFFFF -> Hiproc

  fromEnum s = case s of
    Null    -> 0x00000000 
    Load    -> 0x00000001 
    Dynamic -> 0x00000002 
    Interp  -> 0x00000003 
    Note    -> 0x00000004 
    Shlib   -> 0x00000005 
    Phdr    -> 0x00000006 
    Loos    -> 0x60000000 
    Hios    -> 0x6FFFFFFF 
    Loproc  -> 0x70000000 
    Hiproc  -> 0x7FFFFFFF 

segmentTypeFromWord :: Word32 -> SegmentType
segmentTypeFromWord w = toEnum $ fromIntegral w

parseSegmentType :: Get SegmentType
parseSegmentType = segmentTypeFromWord <$> getWord32le

data Alignment =
    None
  | Aligned Word
  deriving (Eq, Show)

data SegmentFlag =
    Execute
  | Write
  | Read
  | Specific Int
  deriving (Eq, Show)

instance Enum SegmentFlag where
  toEnum s = case s of
    0x1 -> Execute
    0x2 -> Write
    0x4 -> Read
    _ -> Specific s

  fromEnum e = case e of
    Execute       -> 0x1
    Write         -> 0x2
    Read          -> 0x4
    (Specific v)  -> v


parseSegmentFlag :: Get SegmentFlag
parseSegmentFlag = fromWord <$> getWord32le


parseVariableWord :: WordSize -> Get (Either Word32 Word64)
parseVariableWord ws =
    case ws of
        Bit32 -> do
            ep <- getWord32le
            return $ Left ep
        Bit64 -> do
            ep <- getWord64le
            return $ Right ep

data ProgramHeader =
    ProgramHeader {
        getSegmentType :: SegmentType
      , getSegmentFlag :: SegmentFlag
      , getOffset :: Offset
      , getVirtualAddress :: Address
      , getPhysicalAddress :: Address
      , getFileSize :: Size
      , getMemorySize :: Size
      , getAlignment :: Either Word32 Word64
    }


parseProgram :: WordSize -> Endianness -> Get ProgramHeader
parseProgram ws en = do
  st <- parseSegmentType
  flags64 <- parseSegmentFlag
  offset <- parseVariant ws en
  virtualAddress <- parseVariant ws en
  physicalAddress <- parseVariant ws en
  fileSize <- parseVariant ws en
  memorySize <- parseVariant ws en
  flags32 <- parseSegmentFlag
  a <- parseVariableWord ws
  let flags = if ws == Bit64 then flags64 else flags32
  return $ ProgramHeader st flags offset virtualAddress physicalAddress fileSize memorySize a
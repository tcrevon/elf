{-# LANGUAGE DuplicateRecordFields #-}

module Header
( WordSize(..)
, parseHeader
) where

import Elf

import Data.Binary.Get
import Data.Int
import Data.Word


newtype MagicNumber = MagicNumber Word32 deriving (Eq, Show)

parseMagicNumber :: Get MagicNumber
parseMagicNumber = MagicNumber <$> getWord32le

data OSABI =
    SystemV
    | HPUX
    | NetBSD
    | Linux
    | GNUHurd
    | Solaris
    | AIX
    | IRIX
    | FreeBSD
    | Tru64
    | NovellModesto
    | OpenBSD
    | OpenVMS
    | NonStopKernel
    | AROS
    | FenixOS
    | CloudABI
    | Sortix
    deriving (Eq, Show)

instance Enum OSABI where
    toEnum n =
        case n of
            0x00 -> SystemV
            0x01 -> HPUX
            0x02 -> NetBSD
            0x03 -> Linux
            0x04 -> GNUHurd
            0x06 -> Solaris
            0x07 -> AIX
            0x08 -> IRIX
            0x09 -> FreeBSD
            0x0A -> Tru64
            0x0B -> NovellModesto
            0x0C -> OpenBSD
            0x0D -> OpenVMS
            0x0E -> NonStopKernel
            0x0F -> AROS
            0x10 -> FenixOS
            0x11 -> CloudABI
            0x53 -> Sortix
    
    fromEnum n =
        case n of
            SystemV         -> 0x00 
            HPUX            -> 0x01 
            NetBSD          -> 0x02 
            Linux           -> 0x03 
            GNUHurd         -> 0x04 
            Solaris         -> 0x06 
            AIX             -> 0x07 
            IRIX            -> 0x08 
            FreeBSD         -> 0x09 
            Tru64           -> 0x0A 
            NovellModesto   -> 0x0B 
            OpenBSD         -> 0x0C 
            OpenVMS         -> 0x0D 
            NonStopKernel   -> 0x0E 
            AROS            -> 0x0F 
            FenixOS         -> 0x10 
            CloudABI        -> 0x11 
            Sortix          -> 0x53  

parseOSABI :: Get OSABI
parseOSABI = fromWord <$> getWord8

data ObjectType =
    Relocatable
    | Executable
    | Shared
    | Core
    deriving (Eq, Show)

instance Enum ObjectType where
    toEnum n =
        case n of
            1 -> Relocatable
            2 -> Executable
            3 -> Shared
            4 -> Core
    
    fromEnum n =
        case n of
            Relocatable -> 1
            Executable -> 2
            Shared -> 3
            Core -> 4

parseObjectType :: Get ObjectType
parseObjectType = fromWord <$> getWord16le

data Architecture =
    Unspecified
    | SPARC
    | X86
    | MIPS
    | PowerPC
    | S390
    | ARM
    | SuperH
    | IA64
    | X8664
    | AArch64
    | RISCV
    deriving Eq

instance Enum Architecture where
    toEnum n =
        case n of
            0x00 -> Unspecified
            0x02 -> SPARC
            0x03 -> X86
            0x08 -> MIPS
            0x14 -> PowerPC
            0x16 -> S390
            0x28 -> ARM
            0x2A -> SuperH
            0x32 -> IA64
            0x3E -> X8664
            0xB7 -> AArch64
            0xF3 -> RISCV

    fromEnum n =
        case n of
            Unspecified -> 0x00 
            SPARC       -> 0x02 
            X86         -> 0x03 
            MIPS        -> 0x08 
            PowerPC     -> 0x14 
            S390        -> 0x16 
            ARM         -> 0x28 
            SuperH      -> 0x2A
            IA64        -> 0x32 
            X8664       -> 0x3E 
            AArch64     -> 0xB7 
            RISCV       -> 0xF3 

instance Show Architecture where
    show a = case a of
        Unspecified -> "unspecified"
        SPARC -> "SPARC"
        X86 -> "x86"
        MIPS -> "MIPS"
        PowerPC -> "PowerPC"
        S390 -> "S390"
        ARM -> "ARM"
        SuperH -> "SuperH"
        IA64 -> "IA-64"
        X8664 -> "x86-64"
        AArch64 -> "AArch64"
        RISCV -> "RISC-V"

parseArchitecture :: Get Architecture
parseArchitecture = fromWord <$> getWord16le

data Header =
    Header {
        getMagicNumber :: MagicNumber
      , getEndianness :: Endianness
      , getWordSize :: WordSize
      , getElfVersion :: Int8
      , getOSABI :: OSABI
      , getABIVersion :: Int8
      , getObjectType :: ObjectType
      , getArchitecture :: Architecture
      , getEntryPoint :: Address
      , getProgramHeaderTableStart :: Offset
      , getSectionHeaderTableStart :: Offset
      , getFlags :: Word32
      , getHeaderSize :: Word16
      , getProgramHeaderTableEntrySize :: Word16
      , getProgramHeaderTableEntryCount :: Word16
      , getSectionHeaderTableSectionSize :: Word16
      , getSectionHeaderTableSectionCount :: Word16
      , getSectionHeaderTableSectionNamesEntry :: Word16
    } deriving (Eq, Show)

parseHeader :: Get Header
parseHeader = do
    mn <- parseMagicNumber
    ws <- parseWordSize    
    en <- parseEndianness
    ev <- getInt8
    oa <- parseOSABI
    av <- getInt8
    padding <- skip 7
    ot <- parseObjectType
    arch <- parseArchitecture
    versionIgnore <- skip 4
    entrypoint <- parseVariant ws en
    phts <- parseVariant ws en
    shts <- parseVariant ws en
    flags <- getWord32le
    hs <- getWord16le
    htes <- getWord16le
    htec <- getWord16le
    htss <- getWord16le
    htsc <- getWord16le
    htsne <- getWord16le
    return $ Header mn en ws ev oa av ot arch entrypoint phts shts flags hs htes htec htss htsc htsne
